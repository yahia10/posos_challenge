import nltk
import random
from nltk.classify.scikitlearn import SklearnClassifier

from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
from sklearn.model_selection import train_test_split
from nltk.classify import ClassifierI
from statistics import mode

from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer

class VoteClassifier(ClassifierI):
    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)
        return mode(votes)

    def confidence(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)

        choice_votes = votes.count(mode(votes))
        conf = choice_votes / len(votes)
        return conf
        
train_input_file = open(r"data\input_train.csv","r").read()
train_output_file = open(r"data\output_train.csv","r").read()

documents = []
results = []

train_input = train_input_file.split('\n')
train_output = train_output_file.split('\n')
all_words = set()

for i, j in enumerate(train_input):
    if i != 0 and (str(train_output[i][train_output[i].find(',') + 1::]) == str(31) or str(train_output[i][train_output[i].find(',') + 1::]) == str(48) or str(train_output[i][train_output[i].find(',') + 1::]) == str(22) or str(train_output[i][train_output[i].find(',') + 1::]) == str(42)):
        documents.append(j[j.find(',') + 1::])
        results.append(train_output[i][train_output[i].find(',') + 1::])
        words = word_tokenize(j[j.find(',') + 1::])
        for word in words:
            all_words.add(word.lower())

vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(documents)
train_X, val_X, train_y, val_y = train_test_split(X.toarray().tolist(), results, random_state=1)


# Code you have previously used to load data

from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

import pickle
"""
# Specify Model
iowa_model = SVC(random_state=1)
# Fit Model
iowa_model.fit(train_X, train_y)
save_classifier = open("pickled_algos/svm.pickle","wb")
pickle.dump(iowa_model, save_classifier)
save_classifier.close()

open_file = open("pickled_algos/svm.pickle", "rb")
iowa_model = pickle.load(open_file)
open_file.close()
# Make validation predictions and calculate mean absolute error

val_predictions = iowa_model.predict(val_X)



print(accuracy_score(val_predictions, val_y))
"""

from sklearn.naive_bayes import MultinomialNB, BernoulliNB

# Specify Model
iowa_model = MultinomialNB()
# Fit Model
iowa_model.fit(train_X, train_y)
save_classifier = open("pickled_algos/MultinomialNB.pickle","wb")
pickle.dump(iowa_model, save_classifier)
save_classifier.close()

open_file = open("pickled_algos/MultinomialNB.pickle", "rb")
iowa_model = pickle.load(open_file)
open_file.close()
# Make validation predictions and calculate mean absolute error

val_predictions = iowa_model.predict(val_X)



print(accuracy_score(val_predictions, val_y))
from sklearn.ensemble import RandomForestRegressor

# Specify Model
iowa_model = BernoulliNB()
# Fit Model
iowa_model.fit(train_X, train_y)
save_classifier = open("pickled_algos/BernoulliNB.pickle","wb")
pickle.dump(iowa_model, save_classifier)
save_classifier.close()

open_file = open("pickled_algos/BernoulliNB.pickle", "rb")
iowa_model = pickle.load(open_file)
open_file.close()
# Make validation predictions and calculate mean absolute error

val_predictions = iowa_model.predict(val_X)




print(accuracy_score(val_predictions, val_y))


"""
random.shuffle(featuresets)

training_set = featuresets[:int(len(featuresets)*0.75)]
testing_set =  featuresets[int(len(featuresets)*0.75):]
"""
"""
print(int(len(featuresets)*0.75))



classifier = nltk.NaiveBayesClassifier.train(training_set)
print("Original Naive Bayes Algo accuracy percent:", (nltk.classify.accuracy(classifier, testing_set))*100)
classifier.show_most_informative_features(15)

MNB_classifier = SklearnClassifier(MultinomialNB())
MNB_classifier.train(training_set)
print("MNB_classifier accuracy percent:", (nltk.classify.accuracy(MNB_classifier, testing_set))*100)

BernoulliNB_classifier = SklearnClassifier(BernoulliNB())
BernoulliNB_classifier.train(training_set)
print("BernoulliNB_classifier accuracy percent:", (nltk.classify.accuracy(BernoulliNB_classifier, testing_set))*100)

LogisticRegression_classifier = SklearnClassifier(LogisticRegression())
LogisticRegression_classifier.train(training_set)
print("LogisticRegression_classifier accuracy percent:", (nltk.classify.accuracy(LogisticRegression_classifier, testing_set))*100)

SGDClassifier_classifier = SklearnClassifier(SGDClassifier())
SGDClassifier_classifier.train(training_set)
print("SGDClassifier_classifier accuracy percent:", (nltk.classify.accuracy(SGDClassifier_classifier, testing_set))*100)

SVC_classifier = SklearnClassifier(SVC())
SVC_classifier.train(training_set)
print("SVC_classifier accuracy percent:", (nltk.classify.accuracy(SVC_classifier, testing_set))*100)

LinearSVC_classifier = SklearnClassifier(LinearSVC())
LinearSVC_classifier.train(training_set)
print("LinearSVC_classifier accuracy percent:", (nltk.classify.accuracy(LinearSVC_classifier, testing_set))*100)

NuSVC_classifier = SklearnClassifier(NuSVC())
NuSVC_classifier.train(training_set)
print("NuSVC_classifier accuracy percent:", (nltk.classify.accuracy(NuSVC_classifier, testing_set))*100)


voted_classifier = VoteClassifier(
                                  NuSVC_classifier,
                                  LinearSVC_classifier,
                                  MNB_classifier,
                                  BernoulliNB_classifier,
                                  LogisticRegression_classifier)

print("voted_classifier accuracy percent:", (nltk.classify.accuracy(voted_classifier, testing_set))*100)
"""